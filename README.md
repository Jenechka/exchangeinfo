# About

ExchangeInfo is a simple, but very useful Android application. With this tiny app you can get best exchange rates in maximum 3 taps.

Country restrictions: Moldova Republic of only.

# Downloads

You can view all [available downloads](https://bitbucket.org/Jenechka/exchangeinfo/downloads), or download [latest apk file](https://bitbucket.org/Jenechka/exchangeinfo/downloads/ExchangeInfo-1.0-Beta1.apk).

![download-apk-qr-code.gif](https://bitbucket.org/repo/oxXoxL/images/2612827268-download-apk-qr-code.gif)

# Screenshots

![app-1.png](https://bitbucket.org/repo/oxXoxL/images/4071948425-app-1.png)
![app-2.png](https://bitbucket.org/repo/oxXoxL/images/2636265960-app-2.png)
![app-3.png](https://bitbucket.org/repo/oxXoxL/images/400778263-app-3.png)
![app-4.png](https://bitbucket.org/repo/oxXoxL/images/2891932078-app-4.png)